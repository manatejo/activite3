package miage;

/**
 * 
 * @author Nguele ebale manatejo@gmail.com
 * @author minoumisek clauvis
 * 
 *
 */

public class Solveur {
	
	
	
	/************************* fonctions utilitaires ***************************************/
	/**
	 * cherher une valeur sur une ligne de la grille.
	 * @param nombre  le nombre dont on veut vérifier la présence.
	 * @param grille  la grille de Sudoku
	 * @param ligne la ligne sur laquelle on veut faire la recherhe
	 * @return boolean 
	 */
	public boolean absentSurLigne(char nombre, char[][] grille, int ligne ){
		for (int j=0;j<9;j++){
			if(grille[ligne][j] == nombre){
				return true;
			}
		}
		return false;
		
	}
	
	/**
	 *  chercher une valeur sur une colonne de la grille. 
	 * @param nombre le nombre dont on veut vérifier la présence.
	 * @param grille  la grille de Sudoku
	 * @param colonne la colonne sur laquelle on veut faire la recherhe
	 * @return true;
	 */
	public boolean absentSurColonne(char nombre, char[][] grille, int colonne ){
		for (int j=0;j<9;j++){
			if(grille[j][colonne] == nombre){
				return true;
			}
		}
		return false;
		
	}
	/**
	 * Chercher une valeur sur un bloc de la grille.
	 * @param nombre le nombre recherhé
	 * @param grille la grille de sudoku
	 * @param ligne la ligne considérée
	 * @param colonne la colonne considérée 
	 * @return boolean
	 */
	public boolean absentSurBloc (char nombre, char [][] grille , int ligne, int colonne){
		int _i = ligne-(ligne%3);
		int _j = colonne-(colonne%3);
		for (int i=_i;i<_i+3; i++){
			for (int j=_j;j<_j+3; j++){
				if(grille[i][j] == nombre){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Affichage de la grille de sudoku
	 * @param grille
	 */
	public void affichage (char[][] grille){
		for (int i=0; i<9;i++){
			for (int j=0; j<9;j++){
				System.out.print(grille[i][j]+", ");
			}
			System.out.println("\n ");
		}
	}
	
	/************************* fonctions utilitaires ***************************************/
	
	public boolean  estValide (char[][] grille, int position){
	    if (position == 9*9){
	        return true;
	    }
	    int i = position/9, j = position%9;
	    // appel recursif de la méthode
	    if (grille[i][j] != 0){
	        return estValide(grille, position+1);
	    }
	    for (int k=1; k <= 9; k++){
	        if (absentSurLigne((char)k,grille,i) && absentSurColonne((char)k,grille,j) && absentSurBloc((char)k,grille,i,j))

	        {
	            grille[i][j] = (char) k;
	            if ( estValide (grille, position+1) )
	                return true;
	        }
	    }
	    grille[i][j] = '@';
	    return false;

	}
	
	
	
}
